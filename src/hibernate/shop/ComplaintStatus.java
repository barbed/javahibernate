package hibernate.shop;

/**
 * Created by Lukasz on 11.03.2018.
 */
public enum ComplaintStatus {
    PENDING, REJECT, APPROVED
}

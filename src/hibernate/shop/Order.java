package hibernate.shop;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;

/**
 * Created by Lukasz on 10.03.2018.
 */
@Entity
@Table(name = "orders")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"orderDetailList","orderComplaintSet", "orderHistorySet"} )
public class Order implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    BigDecimal totalNetto;
    BigDecimal totalGross;

    @ManyToOne
    @JoinColumn
    User user;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    // Jedno zamowienie moze miec wiele pozycji
    // wlascicielem relacji bedzie order detail
    Set<OrderDetail> orderDetailList;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<OrderHistory> orderHistorySet;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    // join table do laczenia przez tabele dodatkowa
    @JoinTable(
            name = "tabela_order_complaint_history",
            // joinColumns nazwa kolumny w tabeli dodatkowej z kluczem do tabeli laczonej + nazwa pola w encji z kluczem
            // po ktorym laczymy
            joinColumns=@JoinColumn(name="order_complaint_id", referencedColumnName="id"),
            // nazwa kolumny z kluczem glownym z enccji Order
            inverseJoinColumns=@JoinColumn(name="order_id")
    )
    Set<OrderComplaint> orderComplaintSet;


    public Order(BigDecimal totalGross, User user) {
        this.totalGross = totalGross;
        this.user = user;
    }

    public void addOrderDetail(OrderDetail orderDetail){
        orderDetail.setOrder(this);
        orderDetailList.add(orderDetail);
    }

    public void addOrderHistory(OrderHistory orderHistory) {
        orderHistory.setOrder(this);
        this.orderHistorySet.add(orderHistory);
    }

    public OrderHistory getCurrentOrderHistory(){
        return this.getOrderHistorySet().stream().sorted(Comparator.comparing(OrderHistory::getId)).findFirst()
                           .orElse(new OrderHistory());
    }
}

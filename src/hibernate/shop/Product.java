package hibernate.shop;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

/**
 * Created by Lukasz on 10.03.2018.
 */

@Entity
@Data
@EqualsAndHashCode(exclude = {"orderDetailSet", "productRatingSet"})
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "data_dodania")
    private LocalDate date;
    @Enumerated
    private ProductType productType;
    @Embedded
    private Price price;
    private String description;
    @Lob
    private byte[] image;

    //jeden produkt moze byc na wielu pozycji zamowieniach
    @OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
    Set<OrderDetail> orderDetailSet;

    @OneToMany(mappedBy = "product")
    Set<ProductRating> productRatingSet;

    public Product() {
    }

    public Product(String name, ProductType productType, Price price) {
        this.name = name;
        this.productType = productType;
        this.price = price;
        this.date = LocalDate.now();
    }

}

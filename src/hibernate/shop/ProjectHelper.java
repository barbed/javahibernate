package hibernate.shop;

import java.math.BigDecimal;

/**
 * Created by Lukasz on 08.04.2018.
 */
public class ProjectHelper {

    public static Long parseStringToLong(String productId) {
        try {
            return Long.valueOf(productId);
        } catch (NumberFormatException nbf) {
            return 0L;
        }
    }

    public static BigDecimal parseStringToBigDecimal(String productAmount) {
        try {
            return new BigDecimal(productAmount);
        } catch (NumberFormatException nbf) {
            return BigDecimal.ZERO;
        }
    }

    public static Double parseStringToDouble(String productAmount) {
        try {
            return new Double(productAmount);
        } catch (NumberFormatException nbf) {
            return new Double(0);
        }
    }

}
